"use strict"

// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом

// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

let str = prompt("Введите слово палиндром");


function isPalindrome () {
    let strWidth = str.length;
    console.log(strWidth);
    for (let i = 0; i < strWidth / 2; i++) {
        console.log(i);
        if (str[i] !== str[strWidth - 1 - i] ) {
            return console.log(false);
        } 
        }
        return console.log(true);
    }
isPalindrome(str);

// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:

// // Рядок коротше 20 символів

// funcName('checked string', 20); // true

// // Довжина рядка дорівнює 18 символів

// funcName('checked string', 10); // false

let strWidth = prompt("Введите слово не больше 10 символов");
console.log(strWidth.length);

function width10 () {
    if (strWidth.length <= 10) {
        return console.log(true);
    }
    return console.log(false);
}
width10(strWidth);

// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.



function isAge () {
    let date = new Date(prompt("Введите дату рождения через запятую, например 1900 12 12"));
    let nowDate = new Date();

    let miliDate = nowDate.getTime() - date.getTime();
    console.log(miliDate);

    let age = Math.floor(miliDate / (1000 * 60 * 60 * 24 * 365));
    return console.log(age); 


}
isAge();
